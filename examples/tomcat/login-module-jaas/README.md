## Configuration

Install the latest [JAAS](../downloads/jaas-loginmodule-latest.zip) libraries to ``<CATALINA_HOME>/lib``

Add the JAAS Realm in ``<CATALINA_HOME>/conf/server.xml``. You may need to disable/comment out other &lt;Realm&gt;

```xml
<Server port="8005" shutdown="SHUTDOWN">
   ...
  <Service name="Catalina">
    ...
    <Engine name="Catalina" defaultHost="localhost">
        ...
    	<Realm
	    	className="org.apache.catalina.realm.JAASRealm"
		    appName="TestApp"
		    userClassNames="me.yoram.commons.j2se.signin.jaas.UserPrincipal"
		    roleClassNames="me.yoram.commons.j2se.signin.jaas.RolePrincipal"/>
        </Realm>
        ...
    </Engine>
  </Service>
</Server>
```

copy the right jaas file in the conf/*.conf in your CATALINA_HOME/conf folder and apply the right environment variable
as show below.

```bash
# for non md5 password
CATALINA_OPTS=-Djava.security.auth.login.config=$CATALINA_HOME/conf/jaas.conf

# for md5 password
CATALINA_OPTS=-Djava.security.auth.login.config=$CATALINA_HOME/conf/jaas-md5.conf
```

