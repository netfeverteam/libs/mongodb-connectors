<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>me.yoram.signin.mongodb</groupId>
    <artifactId>mongodb-connectors</artifactId>
    <version>4.4.0-SNAPSHOT</version>

    <packaging>pom</packaging>

    <description>
        Signing in to MongoDB 4+ Connectors for J2EE
    </description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- DEPENDENCIES -->
        <dep.mongodb.version>4.4.0</dep.mongodb.version>
        <dep.testng.version>6.14.3</dep.testng.version>

        <!-- PLUGINS -->
        <plugin.spotless.version>2.17.6</plugin.spotless.version>
    </properties>

    <inceptionYear>2018</inceptionYear>

    <licenses>
        <license>
            <name>Apache License</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:git@gitlab.com:netfeverteam/libs/mongodb-connectors.git</connection>
        <developerConnection>scm:git:git@gitlab.com:netfeverteam/libs/mongodb-connectors.git</developerConnection>
        <url>https://gitlab.com/netfeverteam/libs/mongodb-connectors.git</url>
        <tag>HEAD</tag>
    </scm>

    <issueManagement>
        <system>gitlab</system>
    </issueManagement>

    <developers>
        <developer>
            <id>theyoz</id>
            <name>Yoram Halberstam</name>
            <email>yoram.halberstam@gmail.com</email>
        </developer>
    </developers>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>3.0.1</version>
                    <configuration>
                        <failOnError>false</failOnError>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>com.diffplug.spotless</groupId>
                <artifactId>spotless-maven-plugin</artifactId>
                <version>${plugin.spotless.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                        <phase>compile</phase>
                    </execution>
                </executions>
                <configuration>
                    <formats>
                        <format>
                            <includes>
                                <include>*.md</include>
                                <include>.gitignore</include>
                            </includes>
                            <indent>
                                <spaces>true</spaces>
                                <spacesPerTab>4</spacesPerTab>
                            </indent>
                            <trimTrailingWhitespace/>
                            <endWithNewline/>
                        </format>

                        <format>
                            <includes>
                                <include>*.xml</include>
                            </includes>
                            <excludes>
                                <exclude>dependency-reduced-pom.xml</exclude>
                            </excludes>
                            <indent>
                                <spaces>true</spaces>
                                <spacesPerTab>4</spacesPerTab>
                            </indent>
                            <trimTrailingWhitespace/>
                        </format>
                    </formats>
                    <!-- define a language-specific format -->
                    <java>
                        <!-- no need to specify files, inferred automatically, but you can if you want -->
                        <importOrder>
                            <order>me.yoram</order>
                        </importOrder>
                        <removeUnusedImports />
                        <trimTrailingWhitespace />
                        <endWithNewline />
                        <licenseHeader>
                            <file>${user.dir}/spotless/header.txt</file>
                        </licenseHeader>
                        <eclipse>
                            <version>4.18.0</version>
                            <file>${user.dir}/spotless/yoram.me.xml</file>
                        </eclipse>
                    </java>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.0</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                    <!--
                    <debug>true</debug>
                    <debuglevel>none</debuglevel>
                    -->
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencyManagement>
        <dependencies>
            <!-- PROJECT -->
            <dependency>
                <groupId>me.yoram.signin.mongodb</groupId>
                <artifactId>mongodb-commons</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- BUILT -->
            <dependency>
                <groupId>org.mongodb</groupId>
                <artifactId>mongodb-driver-sync</artifactId>
                <version>${dep.mongodb.version}</version>
            </dependency>

            <!-- TEST -->
            <dependency>
                <groupId>org.testng</groupId>
                <artifactId>testng</artifactId>
                <version>${dep.testng.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <modules>
        <module>mongodb-commons</module>
        <module>mongodb-login-module-jaas-connector</module>
        <module>mongodb-realm-connector</module>
    </modules>
</project>